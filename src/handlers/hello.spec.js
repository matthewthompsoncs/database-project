'use strict';
const chai = require('chai');
chai.use(require('chai-as-promised'));

const { expect } = chai;
// const sinon = require('sinon');

const { handler } = require('./hello');
// const { s3Client, sesClient } = require('../libs/aws');

describe('hello', async () => {
  // const sandbox = sinon.createSandbox();

  // beforeEach(() => {
  //   sandbox.restore();
  //   sandbox.stub(s3Client, 'getSignedUrl').returns('signedurl');
  //   sandbox.stub(s3Client, 'deleteObject').returns({
  //     promise: sandbox.stub()
  //   });
  //   sandbox.stub(s3Client, 'headObject').returns({
  //     promise: sandbox.stub().resolves({
  //       Metadata: {
  //         email: 'email@email.com',
  //         filename: 'filename'
  //       }
  //     })
  //   });
  //   sandbox.stub(sesClient, 'sendEmail').returns({
  //     promise: sandbox.stub()
  //   });
  // });

  // after(() => {
  //   sandbox.restore();
  // });

  // it('should format parameters correctly', async () => {
  //   const event = {
  //     Records: [{
  //       s3: {
  //         object: {
  //           key: 'output/dummy'
  //         },
  //         bucket: {
  //           name: 'dummy'
  //         }
  //       }
  //     }]
  //   };
  //   const res = await handler(event);

  //   expect(res.statusCode).deep.equals(200);
  //   sinon.assert.calledOnceWithExactly(s3Client.headObject, {
  //     Bucket: 'dummy',
  //     Key: 'input/dummy'
  //   });
  //   sinon.assert.calledOnceWithExactly(s3Client.deleteObject, {
  //     Bucket: 'dummy',
  //     Key: 'input/dummy'
  //   });
  //   sinon.assert.calledOnceWithExactly(s3Client.getSignedUrl, 'getObject', {
  //     Bucket: 'dummy',
  //     Key: 'output/dummy',
  //     Expires: 604800
  //   });
  //   sinon.assert.calledOnceWithExactly(sesClient.sendEmail, {
  //     Content: {
  //       Simple: {
  //         Body: {
  //           Text: {
  //             Data: 'Signed URL for "filename": signedurl'
  //           }
  //         },
  //         Subject: {
  //           Data: 'Transcoding Job Finished'
  //         }
  //       }
  //     },
  //     Destination: {
  //       ToAddresses: [
  //         'email@email.com'
  //       ]
  //     },
  //     FromEmailAddress: 'email@email.com'
  //   });
  // });

  it('should print Hello World', async () => {
    const event = {
      dummy: 'dummy'
    };
    const res = await handler(event);

    expect(res.statusCode).deep.equals(200);
    expect(JSON.parse(res.body).message).deep.equals('Hello World!');
  });
});
