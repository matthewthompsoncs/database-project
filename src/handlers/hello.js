'use strict';
exports.handler = async (event, context) => {
  const message = {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Hello World!'
    })
  };
  return message;
};
