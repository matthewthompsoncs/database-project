MAKEFLAGS=--warn-undefined-variables

APPLICATION_NAME ?= database-project
ENVIRONMENT_NAME ?= dev
STACK_NAME ?= ${APPLICATION_NAME}-$(ENVIRONMENT_NAME)
DEVOPS_ACCOUNT_ID ?= 283177578178
AWS_REGION ?= us-east-1
ARTIFACT_BUCKET ?= serverless-deployments-${AWS_REGION}-${DEVOPS_ACCOUNT_ID}

test: build lint
	npx mocha './src/{,!(node_modules)/**/}*.js' --timeout 15000

build: node_modules src/node_modules

src/node_modules: src/package.json
	cd src && npm install

node_modules: package.json
	npm install

lint: lint-code lint-cfn

lint-cfn:
	cfn-lint templates/*

lint-code:
	npx eslint './src/{,!(node_modules)/**/}*.js'

package: build artifacts/template.packaged.yaml

artifacts/template.packaged.yaml: templates/*.yaml $(shell find src)
	mkdir -p artifacts
	sam package \
		--template-file ./templates/main.yaml \
		--region ${AWS_REGION} \
		--s3-bucket ${ARTIFACT_BUCKET} \
		--s3-prefix ${APPLICATION_NAME} \
		--output-template-file artifacts/template.packaged.yaml

deploy:
	sam deploy \
		--template-file artifacts/template.packaged.yaml \
		--region ${AWS_REGION} \
		--stack-name ${STACK_NAME} \
		--s3-bucket ${ARTIFACT_BUCKET} \
		--s3-prefix packaged/${APPLICATION_NAME}/${ENVIRONMENT_NAME} \
		--no-fail-on-empty-changeset \
		--parameter-overrides \
			EnvironmentName=${ENVIRONMENT_NAME} \
			ApplicationName=${APPLICATION_NAME} \
		--capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM
